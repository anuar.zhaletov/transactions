# Getting Started
To run application:
```bash 
mvn clean install -DskipTests
mvn spring-boot:run
```

To execute cucumber scenarios:
``` 
mvn test
```

## Documentation
Documentation of API here (Swagger-UI): http://localhost:8081/transactions/swagger-ui/index.html

Endpoint: POST http://localhost:8080/transactions/initialcreate
is idempotent. So if you will send request second time, it will not change anything.
It is good when you want retry mechanism, it will not create additional transactions.