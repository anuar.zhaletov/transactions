package com.capgemini.transactions.models;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class Transaction {
  BigDecimal amount;
  String fromIBAN;
}
