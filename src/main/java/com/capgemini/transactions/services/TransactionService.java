package com.capgemini.transactions.services;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.models.Transaction;
import com.capgemini.transactions.repositories.TransactionRepository;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionService {
    public final TransactionRepository transactionRepository;

    public void save(@NotNull InitialTransactionDTO initialTransactionDTO) {
        transactionRepository.saveInitialTransaction(initialTransactionDTO);
    }

    public List<Transaction> get(@NotNull String customerID) {
        return transactionRepository.getTransactions(customerID);
    }
}
