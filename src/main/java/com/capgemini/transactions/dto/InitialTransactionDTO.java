package com.capgemini.transactions.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class InitialTransactionDTO {
  String customerID;
  BigDecimal initialCredit;
}
