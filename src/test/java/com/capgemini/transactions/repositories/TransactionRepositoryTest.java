package com.capgemini.transactions.repositories;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.exceptions.DisplayClientException;
import com.capgemini.transactions.models.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class TransactionRepositoryTest {
    public TransactionRepository transactionRepository;

    @BeforeEach
    void setUp() {
        this.transactionRepository = new TransactionRepository();
    }

    @Test
    void testSaveInitialTransaction_shouldThrowException_whenCustomerIDIsPresent() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder()
                .initialCredit(BigDecimal.TEN)
                .customerID("CUSTOMER_ID")
                .build();

        transactionRepository.saveInitialTransaction(initialTransactionDTO);

        // populating second time. This request should be idempotent.
        Exception exception = assertThrows(DisplayClientException.class, () -> transactionRepository.saveInitialTransaction(initialTransactionDTO));
        assertThat(exception.getMessage()).isEqualTo("Initial transactions with customerID: CUSTOMER_ID is already set.");
    }

    @Test
    void testGetTransactions() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder()
                .initialCredit(BigDecimal.TEN)
                .customerID("CUSTOMER_ID")
                .build();

        transactionRepository.saveInitialTransaction(initialTransactionDTO);
        List<Transaction> transactions = transactionRepository.getTransactions("CUSTOMER_ID");
        assertThat(transactions).hasSize(1);
        assertThat(transactions.get(0).getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(transactions.get(0).getFromIBAN()).isEqualTo("NL86ABNA01506175536482");
    }
}