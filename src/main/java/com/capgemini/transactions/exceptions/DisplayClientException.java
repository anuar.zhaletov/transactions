package com.capgemini.transactions.exceptions;

public class DisplayClientException extends RuntimeException {
  public DisplayClientException(String message) {
    super(message);
  }
}
