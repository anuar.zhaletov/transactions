package com.capgemini.transactions.services;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.models.Transaction;
import com.capgemini.transactions.repositories.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {
    private TransactionService transactionService;

    @Mock
    private TransactionRepository transactionRepository;

    @BeforeEach
    void setUp() {
        this.transactionService = new TransactionService(transactionRepository);
    }

    @Test
    void testSave() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder()
                .initialCredit(BigDecimal.TEN)
                .customerID("CUSTOMER_ID")
                .build();
        transactionService.save(initialTransactionDTO);
        verify(transactionRepository).saveInitialTransaction(eq(initialTransactionDTO));
    }

    @Test
    void testGet() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder()
                .initialCredit(BigDecimal.TEN)
                .customerID("CUSTOMER_ID")
                .build();
        List<Transaction> transactions = List.of(Transaction.builder()
                .fromIBAN("NL86ABNA01506175536482")
                .amount(BigDecimal.TEN)
                .build());
        when(transactionRepository.getTransactions(eq("CUSTOMER_ID"))).thenReturn(transactions);
        List<Transaction> expectedTransactions = transactionService.get("CUSTOMER_ID");
        assertThat(expectedTransactions).hasSize(1);
        assertThat(expectedTransactions.get(0).getFromIBAN()).isEqualTo("NL86ABNA01506175536482");
        assertThat(expectedTransactions.get(0).getAmount()).isEqualTo(BigDecimal.TEN);
    }
}