package com.capgemini.transactions.repositories;

import com.capgemini.transactions.exceptions.DisplayClientException;
import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.models.Transaction;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class TransactionRepository {
  private static final String FROM_IBAN = "NL86ABNA01506175536482";

  private final Map<String, List<Transaction>> transactionsDB = new HashMap<>();

  private final Object lock = new Object();

  public void saveInitialTransaction(@NotNull InitialTransactionDTO initialTransactionDTO) {
    // synchronized only creating account (not reading). ConcurrentHashMap will not help here as here is multiple steps.
    // if you are using SQL Database synchronized will not require.
    synchronized (lock) {
      if (transactionsDB.get(initialTransactionDTO.getCustomerID()) != null) {
        throw new DisplayClientException("Initial transactions with customerID: " + initialTransactionDTO.getCustomerID() + " is already set.");
      }

      transactionsDB.put(initialTransactionDTO.getCustomerID(), Collections.singletonList(
              Transaction.builder().amount(initialTransactionDTO.getInitialCredit()).fromIBAN(FROM_IBAN).build()));
    }
  }

  public List<Transaction> getTransactions(String customerID) {
    List<Transaction> transactions = this.transactionsDB.get(customerID);
    return transactions == null ? List.of() : transactions;
  }
}
