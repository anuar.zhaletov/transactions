package com.capgemini;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.exceptions.ErrorResponse;
import com.capgemini.transactions.models.Transaction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.List;

import static io.restassured.RestAssured.with;
import static org.assertj.core.api.Assertions.assertThat;

public class StepDefinitions {
  private Response response;

  @Given("user creates transaction with customerID {string} and initialCredit {double}")
  public void userCreatesTransaction(String customerId, double initialCredit) {
    InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder()
        .customerID(customerId)
        .initialCredit(BigDecimal.valueOf(initialCredit))
        .build();
    this.response = with()
        .header("Content-Type", "application/json")
        .body(initialTransactionDTO)
        .when().post("http://localhost:8081/transactions/initialcreate");
  }

  @Then("error response is {int} and {string}")
  public void errorResponse(int statusCode, String errorMessage) {
    response.then().statusCode(statusCode);
    assertThat(response.getBody().as(ErrorResponse.class).getMessageError()).isEqualTo(errorMessage);
    response = null;
  }

  @Then("transaction created for customerID {string}")
  public void accountWithoutTransactions(String customerID) {
    this.response = with()
        .when().get("http://localhost:8081/transactions/" + customerID);
    response.then().statusCode(200);
    List<Transaction> transactions = List.of(response.getBody().as(Transaction[].class));
    assertThat(transactions).hasSize(1);
    assertThat(transactions.get(0).getAmount()).isEqualTo(BigDecimal.valueOf(100.0));
    assertThat(transactions.get(0).getFromIBAN()).isEqualTo("NL86ABNA01506175536482");
    response = null;
  }
//
//  @Then("account created with transaction for customerID {string}")
//  public void accountWithTransactions(String customerID) {
//    this.response = with()
//        .when().get("http://localhost:8080/customers/" + customerID);
//    response.then().statusCode(200);
//    DBEntry dbEntry = response.getBody().as(DBEntry.class);
//    assertThat(dbEntry.getCustomer().getName()).isEqualTo("George");
//    assertThat(dbEntry.getCustomer().getSurname()).isEqualTo("Harrison");
//    assertThat(dbEntry.getAccount().getBalance()).isEqualTo(BigDecimal.valueOf(100.0));
//    assertThat(dbEntry.getTransactions().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(100.0));
//    assertThat(dbEntry.getTransactions().get(0).getFromIBAN()).isEqualTo("NL86ABNA01506175536482");
//  }

}
