package com.capgemini.transactions.controllers;


import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.services.TransactionService;
import com.capgemini.transactions.services.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TransactionControllerTest {
    public TransactionController transactionController;

    @Mock
    private TransactionService transactionService;

    @Mock
    private ValidationService validationService;

    @BeforeEach
    void setUp() {
        this.transactionController = new TransactionController(transactionService, validationService);
    }

    @Test
    void testCreate() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder()
                .customerID("03ea1551-faf6-494e-8108-7a480f31ea8e").initialCredit(BigDecimal.valueOf(100))
                .build();

        transactionController.create(initialTransactionDTO);
        verify(validationService).validate(eq(initialTransactionDTO));
        verify(transactionService).save(eq(initialTransactionDTO));
    }
}