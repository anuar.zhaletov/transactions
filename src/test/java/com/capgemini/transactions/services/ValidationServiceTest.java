package com.capgemini.transactions.services;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ValidationServiceTest {
    private ValidationService validationService;

    @BeforeEach
    void setUp() {
        this.validationService = new ValidationService();
    }

    @Test
    void validateShouldThrowExceptionWhenCustomerIDIsNull() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder().build();
        Exception exception = assertThrows(BadRequestException.class, () -> validationService.validate(initialTransactionDTO));
        assertThat(exception.getMessage()).isEqualTo("CustomerId cannot be null.");
    }

    @Test
    void validateShouldThrowExceptionWhenInitialCreditIsNull() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder().customerID("CUSTOMER_ID").build();
        Exception exception = assertThrows(BadRequestException.class, () -> validationService.validate(initialTransactionDTO));
        assertThat(exception.getMessage()).isEqualTo("InitialCredit cannot be null.");
    }

    @Test
    void validateShouldThrowExceptionWhenInitialCreditIsNegative() {
        InitialTransactionDTO initialTransactionDTO = InitialTransactionDTO.builder().customerID("CUSTOMER_ID").initialCredit(BigDecimal.valueOf(-100)).build();
        Exception exception = assertThrows(BadRequestException.class, () -> validationService.validate(initialTransactionDTO));
        assertThat(exception.getMessage()).isEqualTo("InitialCredit cannot be negative.");
    }
}