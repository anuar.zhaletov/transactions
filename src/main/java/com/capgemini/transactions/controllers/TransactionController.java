package com.capgemini.transactions.controllers;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.models.Transaction;
import com.capgemini.transactions.services.TransactionService;
import com.capgemini.transactions.services.ValidationService;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;
    private final ValidationService validationService;

    @PostMapping(value = "initialcreate", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(@RequestBody InitialTransactionDTO initialTransactionDTO) {
        validationService.validate(initialTransactionDTO);
        transactionService.save(initialTransactionDTO);
    }

    @GetMapping(value = "{customerID}")
    @ResponseBody
    public List<Transaction> getTransactions(@PathVariable String customerID) {
        return transactionService.get(customerID);
    }
}
