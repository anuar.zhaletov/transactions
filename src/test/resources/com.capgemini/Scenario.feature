Feature: An example

  Scenario: System is creating initial transaction for customer
    When user creates transaction with customerID "03ea1551-faf6-494e-8108-7a480f31ea8e" and initialCredit 100
    Then transaction created for customerID "03ea1551-faf6-494e-8108-7a480f31ea8e"

  Scenario: User is creating initial transaction for customer which already have initial transaction
    When user creates transaction with customerID "03ea1551-faf6-494e-8108-7a480f31ea8e" and initialCredit 200
    Then error response is 500 and "Initial transactions with customerID: 03ea1551-faf6-494e-8108-7a480f31ea8e is already set."

  Scenario: User is creating transaction with negative value
    When user creates transaction with customerID "03ea1551-faf6-494e-8108-7a480f31ea8e" and initialCredit -100
    Then error response is 400 and "InitialCredit cannot be negative or equal to zero."

  Scenario: User is creating transaction with zero balance
    When user creates transaction with customerID "08b223a0-2ea2-4c51-ae02-d1cc659ac663" and initialCredit 0
    Then error response is 400 and "InitialCredit cannot be negative or equal to zero."