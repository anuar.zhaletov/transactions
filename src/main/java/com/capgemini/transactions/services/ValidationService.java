package com.capgemini.transactions.services;

import com.capgemini.transactions.dto.InitialTransactionDTO;
import com.capgemini.transactions.exceptions.BadRequestException;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ValidationService {
  public void validate(@NotNull InitialTransactionDTO initialTransactionDTO) {
    if (initialTransactionDTO.getCustomerID() == null) {
      throw new BadRequestException("CustomerId cannot be null.");
    }
    if (initialTransactionDTO.getInitialCredit() == null) {
      throw new BadRequestException("InitialCredit cannot be null.");
    }
    if (initialTransactionDTO.getInitialCredit().compareTo(BigDecimal.ZERO) <= 0) {
      throw new BadRequestException("InitialCredit cannot be negative or equal to zero.");
    }
  }
}
